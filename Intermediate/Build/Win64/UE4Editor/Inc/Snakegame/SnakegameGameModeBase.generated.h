// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SnakegameGameModeBase_generated_h
#error "SnakegameGameModeBase.generated.h already included, missing '#pragma once' in SnakegameGameModeBase.h"
#endif
#define SNAKEGAME_SnakegameGameModeBase_generated_h

#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_SPARSE_DATA
#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_RPC_WRAPPERS
#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakegameGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakegameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakegameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snakegame"), NO_API) \
	DECLARE_SERIALIZER(ASnakegameGameModeBase)


#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASnakegameGameModeBase(); \
	friend struct Z_Construct_UClass_ASnakegameGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ASnakegameGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snakegame"), NO_API) \
	DECLARE_SERIALIZER(ASnakegameGameModeBase)


#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakegameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakegameGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakegameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakegameGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakegameGameModeBase(ASnakegameGameModeBase&&); \
	NO_API ASnakegameGameModeBase(const ASnakegameGameModeBase&); \
public:


#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakegameGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakegameGameModeBase(ASnakegameGameModeBase&&); \
	NO_API ASnakegameGameModeBase(const ASnakegameGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakegameGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakegameGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakegameGameModeBase)


#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_12_PROLOG
#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_SPARSE_DATA \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_RPC_WRAPPERS \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_INCLASS \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_SPARSE_DATA \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Snakegame_Source_Snakegame_SnakegameGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASnakegameGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snakegame_Source_Snakegame_SnakegameGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
