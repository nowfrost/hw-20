// Copyright Epic Games, Inc. All Rights Reserved.

#include "Snakegame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Snakegame, "Snakegame" );
